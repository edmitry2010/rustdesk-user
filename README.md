# Установка RustDest в Docker от пользователя

## Данная документация, написана для Debian, но тут разница с другими ОС, только в установке Docker и расположение файла /etc/nftables.conf. На официальном сайте Docker, есть документация для любой ОС.

На сервере, потребуются некоторые зависимости, ставим
```
apt-get update; apt-get -y install git
```

## Ставим Docker
Официальная документация для: \
[Debian](https://docs.docker.com/engine/install/debian/),
[Ubuntu](https://docs.docker.com/engine/install/ubuntu/)

Ставить буду от root, по этой причине sudo не потребуется

Удаляем если установлен
```
for pkg in docker.io docker-doc docker-compose podman-docker containerd runc; do apt-get remove $pkg; done
```

Добавление GPG ключей:
```
apt-get update; apt-get -y install ca-certificates curl
install -m 0755 -d /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/debian/gpg -o /etc/apt/keyrings/docker.asc
chmod a+r /etc/apt/keyrings/docker.asc
```

Добавление официального репозитория
```
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/debian \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
  tee /etc/apt/sources.list.d/docker.list > /dev/null
```

Установка Docker
```
apt-get update; apt-get -y install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
```

Проверяем службу Docker
```
systemctl status docker
```

В случае, если служба будет disable, включаем и активируем автозагрузку
```
systemctl enable --now docker
```

## Ставим RustDesk

Переходим в удобную для вас папочку, например
```
cd /opt
```

Клонируем текущий репозиторий
```
git clone https://gitlab.com/edmitry2010/rustdesk-user.git
```

Переходим в папочку (репозиторий)
```
cd rustdesk-user
```

Создаём пользователя, без возможности авторизации
```
useradd -rs /bin/false rustdesk
```

Создаём папочку для ключей и базы
```
mkdir data
```

Назначим права на папочку
```
chown rustdesk:rustdesk data
```

Смотрим пользователя **rustdesk**, намнужны **uid** и **gid**
```
cat /etc/passwd | grep rustdesk
```
В моём случае это
```
999:995
```

Правим пользователя в **docker-compose.yml**, указваем ваши цифры в кавычках, после **user:**
```
nano docker-compose.yml
```
```yml
version: '3'

services:
  hbbs:
    container_name: rustdesk-hbbs
    image: rustdesk/rustdesk-server:latest
    user: "999:995"
    ...

  hbbr:
    container_name: rustdesk-hbbr
    image: rustdesk/rustdesk-server:latest
    user: "999:995"
    ...
```

Запуск нашего **docker-compose.yml**

Если название файла именно так и написано **docker-compose.yml**, тогда не потребуется указывать название.
```
docker compose up -d
```

Проверяем
```
docker ps
```
> **Результат будет примерно таким. CONTAINER ID и CREATED будут уже вашими**
```
CONTAINER ID   IMAGE                             COMMAND                  CREATED      STATUS             PORTS                                       NAMES
11342000b8d8   rustdesk/rustdesk-server:latest   "hbbs -r rustdesk.dm…"   8 days ago   Up About an hour                                               rustdesk-hbbs
3c4a81ffee04   rustdesk/rustdesk-server:latest   "hbbr"                   8 days ago   Up About an hour                                               rustdesk-hbbr
```

Публичный и закрытый ключи, будут лежать в папочке /opt/rustdesk-user/data/, там же и база данных.

Что бы посмотреть его и передать кому нужно.
```
cat /opt/rustdesk-user/data/id_ed25519.pub; echo
```

## Настроим nftables (файрвол)

На всякий случай, надо поставить **nftables**, у некоторых хостеров в шаблонах не везде он предустановлен сразу.
```
apt-get update && apt-get -y install nftables
```

В Debian и Ubuntu, конфигурационный файл **nftables.conf**, по умолчанию находится в **/etc/nftables.conf**

Я заранее приготовил файл **nftables.conf**. \
В этом конфигурационном файле, открыты **tcp/udp** порты **ssh** и **RustDesk** (**22, 21115-21119**), можно просто заменить текущий в системе.

> **Внимание:** Если вы не планируете больше ни чего настраивать на этом сервере, тогда этот вариант вам подходит, если вам потребуется ещё открыть порты 80 и 443, просто допишите их через запятую.

Меняем системный конфиг, на наш.
```
cp nftables.conf /etc/nftables.conf
```
> Возможно спросит замену файла, т.к. он уже существует, отвечаем положительно.

В системе по умолчанию, **nftables** может быть не включенным, включаем и активируем автозапуск одной командой.
```
systemctl enable --now nftables.service
```

> **Результат, если небыл включен, если был включен, тогда не будет ни чего**
```
Created symlink /etc/systemd/system/sysinit.target.wants/nftables.service → /lib/systemd/system/nftables.service.
```

Однако, если он был уже запущен, тогда нам потребуется его перезапустить.
```
systemctl restart nftables.service
```

Проверяем работу **службы nftables**
```
systemctl status nftables.service
```
> **Результат**
```
nftables.service - nftables
     Loaded: loaded (/lib/systemd/system/nftables.service; enabled; preset: enabled)
     Active: active (exited) since Fri 2024-04-26 08:44:49 UTC; 1min 6s ago
       Docs: man:nft(8)
             http://wiki.nftables.org
    Process: 2342 ExecStart=/usr/sbin/nft -f /etc/nftables.conf (code=exited, status=0/SUCCESS)
   Main PID: 2342 (code=exited, status=0/SUCCESS)
        CPU: 120ms

```

Проверяем работу **nftables**
```
nft list ruleset
```

> **Результат**
```
table inet MyTables {
        chain input {
                type filter hook input priority filter; policy drop;
                ct state vmap { invalid : drop, established : accept, related : accept, untracked : accept } comment "Разрешим established,related и удалим invalid, входящие"
                meta l4proto { tcp, udp } th dport { 22, 21115-21119 } ct state new counter packets 0 bytes 0 accept comment "Разрешим TCP/UDP"
        }
}
```

Готово, всё должно работать.

## Обновление RustDesk сервера

Переходим в папочку с файлом **docker-compose.yml** (репозиторий)
```
cd /opt/rustdesk-user
```

Останавливаем контейнеры
```
docker compose down
```

В результате, он удалит контейнеры и оставит папочку **data** с вашим ключом не тронутой. Но оставит образ контейнера который скачал во время установки.

Смотрим образы
```
docker images
```
> **Результат**
```
REPOSITORY                 TAG       IMAGE ID       CREATED        SIZE
rustdesk/rustdesk-server   latest    9a7dedfd1994   2 months ago   23.5MB
```

В моём случае, **IMAGE ID** будет **9a7dedfd1994**, очень удобно удалять именно по **IMAGE ID**, а иногда иначе ни как).

Удаляем образ
```
docker rmi 9a7dedfd1994
```

Всё, теперь можно просто опять запустить сборку **docker-compose.yml**, docker сам скачает свежий образ и создаст нужные контейнеры с вашим старым ключом.
```
docker compose up -d
```
Готово, обновились.

## Обновить только ключи
В случае, если потребуется обновить ключи, можно просто остановить контейнеры, удалить текущие ключи и запустить контейнеры.
```
docker compose down
```
```
rm -f /opt/rustdesk-user/data/id_ed25519*
```
```
docker compose up -d
```

Готово, ключи будут созданы новые.

Смотрим новый публичный ключ.
```
cat /opt/rustdesk-user/data/id_ed25519.pub; echo
```
